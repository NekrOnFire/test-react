import React from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import Post from "./post";

export const PostList = ({ posts, title, remove }) => {

    if (!posts.length) {
        return (
            <h1 style={{ textAlign: 'center ' }}>
                Постов нет!
            </h1>
        )
    }

    return (
        <div>
            <h1 style={{ textAlign: "center" }}>{title}</h1>
            <TransitionGroup>
                {posts.map((post, index) =>
                    <CSSTransition
                        key={post.id}
                        timeout={500}
                        classNames="post"
                    >
                        <Post remove={remove} post={post} number={index + 1} />
                    </CSSTransition>
                )}
            </TransitionGroup>

        </div>
    )
}