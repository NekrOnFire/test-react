import React, { useState } from 'react'
import Button from './UI/button/button'
import Input from './UI/input/input'

const PostForm = ({ create }) => {
    const [post, setPost] = useState({
        title: '',
        body: '',
    });

    const addNewPost = (evt) => {
        evt.preventDefault();

        const newPost = {
            ...post,
            id: Date.now()
        }
        create(newPost);
        setPost({
            title: '',
            body: '',
        })
    }

    return (
        <div>
            <form>
                <Input
                    value={post.title}
                    onChange={(evt) => setPost({ ...post, title: evt.target.value })}
                    type="text"
                    placeholder="Название поста" />
                <Input
                    value={post.body}
                    onChange={(evt) => setPost({ ...post, body: evt.target.value })}
                    type="text"
                    placeholder="Описание поста" />
                <Button onClick={addNewPost}>Создать пост</Button>
            </form>
        </div>
    )
}

export default PostForm