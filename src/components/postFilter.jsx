import React from 'react'
import Input from './UI/input/input'
import Select from './UI/select/select'

const PostFilter = ({ filter, setFilter }) => {
  return (
    <div>
      <Input
        value={filter.query}
        onChange={evt => setFilter({ ...filter, query: evt.target.value })}
        placeholder="Поиск..." />

      <Select
        value={filter.sort}
        onChange={selectedSort => setFilter({ ...filter, sort: selectedSort })}
        defaultValue="Сортировка"
        options={[
          { value: 'title', name: 'По названию' },
          { value: 'body', name: 'По описанию' },
        ]} />
    </div>
  )
}

export default PostFilter