import React, { useContext } from 'react'
import Button from '../components/UI/button/button'
import Input from '../components/UI/input/input'
import { AuthContext } from '../context'

const Login = () => {
    const {isAuth, setIsAuth} = useContext(AuthContext)
    const login = (evt) => {
        evt.preventDefault();
        setIsAuth(true);
        localStorage.setItem('auth', 'true')
    }

    return (
        <div>
            <h1>Страница для ввода логина</h1>
            <form onSubmit={login}>
                <Input type="text" placeholder="Введите логин" />
                <Input type="password" placeholder="Введите пароль" />
                <Button>Войти</Button>
            </form>
        </div>
    )
}

export default Login