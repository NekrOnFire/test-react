import React, { useEffect, useRef, useState } from "react";
import PostService from "../API/postService";
import PostFilter from "../components/postFilter";
import PostForm from "../components/postForm";
import { PostList } from "../components/postList";
import Button from "../components/UI/button/button";
import Loader from "../components/UI/loader/loader";
import Modal from "../components/UI/modal/modal";
import Pagination from "../components/UI/pagination/pagination";
import Select from "../components/UI/select/select";
import { useFetching } from "../hooks/useFetching";
import { useObserver } from "../hooks/useObserver";
import { usePosts } from "../hooks/usePosts";
import "../styles/app.css";
import { getPageCount } from "../utils/pages";

function Posts() {
    const [posts, setPosts] = useState([])

    const createPost = (newPost) => {
        setPosts([...posts, newPost]);
        setModal(false);
    }

    const removePost = (post) => {
        setPosts(posts.filter(p => p.id !== post.id))
    }

    const [filter, setFilter] = useState({ sort: '', query: '' });
    const [modal, setModal] = useState(false);
    const [totalPages, setTotalPages] = useState(0);
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);
    const sortedAndSearchedPosts = usePosts(posts, filter.query, filter.sort);
    const lastElement = useRef();

    const [fetchPosts, isPostsLoading, postError] = useFetching(async () => {
        const response = await PostService.getAll(limit, page);
        setPosts([...posts, ...response.data])
        const totalCount = response.headers['x-total-count']
        setTotalPages(getPageCount(totalCount, limit));
    })

    useObserver(lastElement, page < totalPages, isPostsLoading, () => {
        setPage(page + 1);
    })

    const changePage = (page) => {
        setPage(page)
    }

    useEffect(() => {
        fetchPosts(page, limit);
    }, [page, limit])

    return (

        <div className="App">
            <Button style={{ marginTop: 25 }} onClick={() => setModal(true)}>
                Создать пользователя
            </Button>
            <Modal visible={modal} setVisible={setModal}>
                <PostForm create={createPost} />
            </Modal>
            <hr style={{ margin: '15px 0' }} />
            <PostFilter
                filter={filter}
                setFilter={setFilter}
            />
            <Select
                value={limit}
                onChange={value => setLimit(value)}
                defaultValue='Постов на странице:'
                options={[
                    { value: 5, name: '5' },
                    { value: 10, name: '10' },
                    { value: 25, name: '25' },
                    { value: -1, name: 'Показать все' }
                ]}
            />
            {postError &&
                <h1>Произошла ошибка ${postError}</h1>
            }
            <PostList remove={removePost} posts={sortedAndSearchedPosts} title="Посты про JS" />
            <div ref={lastElement} style={{ height: 30, background: 'red' }}></div>
            {isPostsLoading &&
                <div style={{ display: 'flex', justifyContent: 'center', marginTop: 70 }}> <Loader /></div>
            }

            <Pagination page={page} totalPages={totalPages} changePage={changePage} />
        </div>
    );
}

export default Posts;